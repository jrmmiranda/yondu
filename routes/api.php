<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//List Users
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');

//List Single User
Route::get('/users/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');

//Create Users
Route::get('/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');

//Store Users
Route::post('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');

//Update Users
Route::put('/users/update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');

//Delete Users
Route::delete('/users/{id}/delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');

//Delete Multiple Users
Route::post('/users/delete/{id}', [App\Http\Controllers\UserController::class, 'destroyMultiple'])->name('users.destroyMultiple');

