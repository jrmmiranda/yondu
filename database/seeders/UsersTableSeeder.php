<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
        	'first_name' => "Janrald Rae",
            'last_name' => "Miranda",
            'username' => "jrmmiranda",
            'email' => "mirandajanraldrae@yondu.com",
            'password' => bcrypt("yonduFakePassword"),
        ]);
        
        User::factory()->count(15)->create();
    }
}
