## Quick Start
- # Install Dependencies
composer install

- # Creating a .env file
Create a .env file using the content of .env.example

- # Create database
Create a database with the name "yondu", username "root", and no password

- # Run Migrations
php artisan migrate

- # Import Users
php artisan db:seed

- # Create personal client key
php artisan passport:install

- # If you get an error about an encryption key
php artisan key:generate

- # Install Javascript Dependencies
npm install

- # Watch files
npm run watch

- # Run application
php artisan serve


## API Endpoints
List all Users
- GET api/users

Get single user
- GET api/users/{id}

Delete single user
- DELETE api/users/{id}/delete

Create and store user
- POST api/users/store

Update user
- PUT api/users/update/{id}

Delete Multiple Users
- POST api/users/delete/{id}


## Admin Initial Authentication
Email
- mirandajanraldrae@yondu.com

Password
- yonduFakePassword


## Application Help
- # Top Form
The form directly under the "Users" label is used as a Create User form upon page load and reload, and used as an Edit User form 
when an "Edit" button located on the user cards is clicked. When an "Edit Button" is clicked, the top form gets filled with the
selected user's information. The top form CAN ONLY BE USED AS A CREATE USER FORM after page reload, or after a successful add,
edit, or delete operation.

- # Multiple Deletes
Tick the checkboxes beside each card's header to select the users you wish to delete.


## Laravel Version
- Laravel 8.10.0


## PHP Version
- PHP 7.4.10 (cli)