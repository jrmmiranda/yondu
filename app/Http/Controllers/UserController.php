<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get Users
        $users = User::orderBy('created_at', 'desc')->paginate(5);

        //Return collection of users as resource
        return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);

        $user = User::create($validated);
        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['user' => $user, 'access_token' => $accessToken]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get a single user
        $user = User::findOrFail($id);

        //Return single user as resource
        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id)
    {
        $user = User::findOrFail($id);

        $emailValidate = $request->validate([
            'email' => 'email|required|unique:users,email,'.$id
        ]);
        $email = $emailValidate['email'];

        $validated = $request->validated();

        if($validated['password'] != " "){
            $validated['password'] = bcrypt($validated['password']);
        }

        $user->first_name = $validated['first_name'];
        $user->last_name = $validated['last_name'];
        $user->address = $validated['address'];
        $user->postcode = $validated['postcode'];
        $user->contact_phone_number = $validated['contact_phone_number'];
        $user->email = $email;
        $user->username = $validated['username'];
        if($validated['password'] != " "){
            $user->password = $validated['password'];
        }

        if($user->save()){
            return new UserResource($user);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get a single user
        $user = User::findOrFail($id);

        if($user->delete()){
            //Return single user as resource
            return new UserResource($user);
        }
    }

    public function destroyMultiple($id)
    {
        $userIds = explode(',' , $id);

        foreach($userIds as $id){
            $user = User::findOrFail($id)->delete();
        }
    }
}
